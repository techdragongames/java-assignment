package Assignment;

public class Vehicle implements Comparable<Vehicle> {

    String type;
    double speed;

    public Vehicle() {
        type = "bicycle";
        speed = 20.0;
    }
    public Vehicle(String type, double speed) {
        this.type = type;
        this.speed = speed;
    }

    public void setType(String type) {
        type = this.type;
    }

    public String getType() {
        return type;
    }

    public void setSpeed(double speed) {
        speed = this.speed;
    }

    public double getSpeed() {
        return speed;
    }

    public void speedUp() {
        speed = speed * 2;
    }

    public void speedDown() {
        speed = speed / 2;
    }

    @Override public int compareTo(Vehicle v) {

        if (this.type.equals(v.getType()) && this.speed == v.getSpeed()) {
            return 0;
        }
        else if (!this.type.equals(v.getType()) && this.speed == v.getSpeed()){
            return -1;
        } else {
            return 1;
        }
    }
}
