package Assignment;

import javax.swing.*;
import java.awt.*;

public class BelishaBeacon extends JFrame {

    private BeaconPanel beaconPanel;
    private Timer flashTimer;
    public BelishaBeacon(String title) {
        super(title);

        // Set up the layout manager
        setLayout(new GridBagLayout());

        GridBagConstraints gc = new GridBagConstraints();

        // Create the swing components
        beaconPanel = new BeaconPanel();

        // Setting up the timer, this MUST be set to the beaconPanel
        flashTimer = new Timer(500, e -> beaconPanel.repaint());

        // Create the Flash button
        JButton BTN_Flash = new JButton("Flash");
        // Add an action listener that will re-start the timer when clicked
        BTN_Flash.addActionListener(e -> flashTimer.start());

        // Create the steady button
        JButton BTN_Steady = new JButton("Steady");
        // Add an action listener that will stop the timer when clicked
        BTN_Steady.addActionListener(e -> {
            flashTimer.stop();
            beaconPanel.repaint();
            beaconPanel.isFlashing = false;
        });

        // First Column //
        gc.anchor = GridBagConstraints.CENTER;
        gc.weighty = 10;
        gc.gridx = 0;
        gc.gridy = 0;
        add(beaconPanel, gc);

        gc.anchor = GridBagConstraints.LINE_START;
        gc.weighty = 2;
        gc.weightx = 10;
        gc.gridx = 0;
        gc.gridy = 1;
        add(BTN_Flash, gc);

        gc.anchor = GridBagConstraints.LINE_END;
        gc.gridx = 0;
        gc.gridy = 1;
        add(BTN_Steady, gc);

        // Start the timer
        flashTimer.start();
    }

    public static void main(String[] args) {
        // Setting up a separate thread for the frame to run on
        // This is being setup using a lambda expression
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new BelishaBeacon("Belisha Beacon"); // Make a new frame and set the title
            frame.setSize(320, 500); // Set it's width and height
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Make the application terminate on close
            frame.setVisible(true); // Make it visible
        });
    }
}
