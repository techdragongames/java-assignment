package Assignment;


import java.util.LinkedList;
import java.util.Scanner;

public class Exercise3 {

    public static boolean isNarcissisticNumber(int number) {
    /*
    * This Function tells the user weather the number they enter is narcissistic or not.
    *
    *  @Params
    *   int Original_Number :   This stores the original number passed into the method.
    *   String number_string    :   This converts number into a string.
    *   int length  :   This is the length of number_string, giving the length of number.
    *   LinkedList Nums_Stack   :   This will store the individual digits from number.
    *   int answer  :   This will store an int value given after some calculations.
    *   boolean isNarcissistic  :   Will remain false unless answer is the same as number.
    * */

//    Original number
        int Original_Number = number;

//    This part obtains the length of the number
        String number_string = Integer.toString(number);
        int length = number_string.length();

//     This creates a new LinkedList that will store the values of the individual digits
        LinkedList<Integer> Nums_Stack = new LinkedList<>();
        int answer = 0; // This will store the final answer
        boolean isNarcissistic = false;

        while (number > 0) {
            // Go through each number and push it into Nums_Stack, then set number to the left over values.
            Nums_Stack.push( number % 10);
            number = number / 10;
        }
        for (int i = 0; i < Nums_Stack.size();i++) {
            // Go through each element in Nums_Stack and complete a calculation, storing the answer in the answer var.
            answer += Math.pow(Nums_Stack.get(i),length);
        }
        if (answer == Original_Number) {
            isNarcissistic = true;
        }

        return isNarcissistic;
    }

    public static int sum() {

        LinkedList<Integer> Narcissistic_Numbers = new LinkedList<>();
        for (int x = 10; x < 1000; x++) {
            int origin_value = x;
            LinkedList<Integer> values = new LinkedList<>();

            while (origin_value > 0) {
                values.push(origin_value % 10);
                origin_value = origin_value / 10;
            }
            int answer = 0;
            for (Integer nums :
                    values) {
                answer += Math.pow(nums, values.size());
            }
            if (x == answer) {
                Narcissistic_Numbers.push(x);
            }
        }
        int sum = 0;
        for (int y = 0; y < Narcissistic_Numbers.size(); y++) {
            sum += Narcissistic_Numbers.get(y);
        }
        return sum;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a number");
        int user_number = input.nextInt();
        System.out.print(isNarcissisticNumber(user_number));
//        System.out.println(sum());
    }
}
