package Assignment;

import java.util.Random;
import java.util.Scanner;

public class Exercise2 {

    public static void partA() {

        int score = 0;
        int total = 0;

        for (int x = 1; x <= 5;x++) {
            Random rand_int = new Random();

//          Creates two vars, both assigned with a random number in the range 10 - 20 (inclusive)
            int rand_int1 = rand_int.nextInt(11) + 10;
            int rand_int2 = rand_int.nextInt(11) + 10;

            int answer = rand_int1 % rand_int2;

            System.out.println(rand_int1 + " % " + rand_int2 + "?\nEnter below");
            Scanner input = new Scanner(System.in);
            int user_answer = input.nextInt();
            if (user_answer == answer) {

                score = score + 20;
                total = total + 1;

                System.out.println("Correct!, Score: " + score + ", Performance: " + total + "/" + x);
            }else {
                System.out.println("Incorrect!, Score: " + score + ", Performance: " + total + "/" + x);
            }
        }
    }

    public static void partB() {

        int score = 0;
        int total = 0;
        int questions_asked = 0;

        while (true) {
            Random rand_int = new Random();
            questions_asked += 1; // increase the number of questions asked by one

//          Creates two vars, both assigned with a random number in the range 10 - 20 (inclusive)
            int rand_int1 = rand_int.nextInt(11) + 10;
            int rand_int2 = rand_int.nextInt(11) + 10;
            int answer = rand_int1 % rand_int2;
            System.out.println(rand_int1 + " % " + rand_int2 + "?\nEnter below (enter 'q' to exit)");
            Scanner input = new Scanner(System.in);
            if (input.hasNextInt()) {
                int user_answer = input.nextInt();
                if (user_answer == answer) {
                    score = score + 20;
                    total = total + 1;

                    System.out.println("Correct!, Score: " + score + ", Performance: " + total + "/" + questions_asked);
                } else {
                    System.out.println("Incorrect!, Score: " + score + ", Performance: " + total + "/" + questions_asked);
                }
            }
            else if (input.hasNext("q")) {
                break;
            }
            else {
                System.out.println("Incorrect input!");
            }

        }
    }

    public static void main(String[] args) {
        partB();
    }
}
