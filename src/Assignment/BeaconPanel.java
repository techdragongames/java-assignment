package Assignment;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class BeaconPanel extends JPanel {
    /*
    * @author Callum-James Smith
    *
    * @see #BeconPanel
    * This class is responsible for drawing 2D graphics inside the JFrame.
    * */

    public boolean isFlashing = true;

    public BeaconPanel() {
	
        Dimension size = getPreferredSize();
        size.width = 300;
        size.height = 400;
        setPreferredSize(size);
        setBorder(BorderFactory.createTitledBorder("Beacon"));
    }

    public void paint(Graphics g) {

        super.paint(g);
        Graphics2D graph2 = (Graphics2D)g;

        graph2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // Anti-Aliasing

        // The pole, or rectangle
        Shape drawRect = new Rectangle2D.Float(145,148,10,230);
        Shape drawStripes_1 = new Rectangle2D.Float(145, 150, 10, 20);
        Shape drawStripes_2 = new Rectangle2D.Float(145, 200, 10, 20);
        Shape drawStripes_3 = new Rectangle2D.Float(145, 250, 10, 20);
        Shape drawStripes_4 = new Rectangle2D.Float(145, 300, 10, 20);
        Shape drawStripes_5 = new Rectangle2D.Float(145, 350, 10, 20);

        // Draw the shape
        graph2.draw(drawRect); // then draw the rectangle, this is drawn first
        // Colour the stripes
        graph2.setColor(Color.BLACK);
        // Then draw the stripes using fill
        graph2.fill(drawStripes_1);
        graph2.fill(drawStripes_2);
        graph2.fill(drawStripes_3);
        graph2.fill(drawStripes_4);
        graph2.fill(drawStripes_5);

        // Colour for the circle ('ball')
        graph2.setColor(Color.ORANGE);
        // Draw it
        graph2.fill(new Ellipse2D.Double(100, 50, 100, 100)); // then this is drawn next, *will be on top of rectangle*

        if (isFlashing) {
            graph2.setColor(Color.darkGray);
            graph2.fill(new Ellipse2D.Double(100, 50, 100, 100));
        }
        isFlashing = !isFlashing; // negates isFlashing

    }

}
