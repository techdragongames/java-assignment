package Assignment;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class WordFinder {

    private static final int N = 10;
    public static char[][] grid = new char[N][N];
    public static final String GRID_FILE = "data/grid.txt";
    public static final String WORD_FILE = "data/words.txt";


    public static void find() throws IOException {

        int row = 0;
        int col = 0;
        String word, check="";
        ArrayList<String> word_list = new ArrayList<>();

        try {
            Scanner file = new Scanner(new File(WORD_FILE));
            while(file.hasNextLine()) {
                word = file.nextLine();
                word_list.add(word);
            }
            // Horizontal
            while (row < 10) {
                while (col < 10) {
                    check += grid[row][col];
                    col += 1;
                }
                for (String wrd : word_list) {
                    if (check.contains(wrd)) {
                        System.out.println(wrd + ": Row " + row + ", position " + check.indexOf(wrd));
                    }
                }
                col = 0;
                row += 1;
                check = "";
            }
            // Vertical
            row = 0;
            while (col < 10) {
                while (row < 10) {
                    check += grid[row][col];
                    row += 1;
                }
                for (String wrd : word_list) {
                    if (check.contains(wrd)) {
                        System.out.println(wrd + ": Column " + col + ", position " + check.indexOf(wrd));
                    }
                }
                row = 0;
                col += 1;
                check = "";
            }

        } catch (IOException e) {
            System.out.println("File I/O Error! " + e);
        }
    }

    public static void main(String[] args) throws IOException {
        find();
//        initGrid();
//        printGrid();
    }
}
