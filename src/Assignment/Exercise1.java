package Assignment;

import java.util.Arrays;

/*
 * Exercise 1
 * */
public class Exercise1 {

    public static int mainDiagonalElementSum(int[][] array) {
        // Sum up the first element of [0], then the 2nd of [1] then 3rd of [2] etc.
        int i = 0;
        int sum = 0;
        for (i = 0; i < array.length;i++) {
            sum = sum + array[i][i];
        }
        return sum;

    }

    public static int maxRowAbsSumValue(int[][] array) {
        /*
         * Compute sum of the absolute values in each row, then display the answer from the highest row (answer)
         */
        int sum[] = new int[array.length];
        int answer = 0;

        for (int x = 0; x<array.length;x++) {
            sum[x] = 0;
            for (int i = 0; i<array[x].length;i++) {
                sum[x] += Math.abs(array[x][i]);
            }
        }
        for (int y = 0, j = 1; y < sum.length && j < sum.length;y++,j++) {
            if (sum[y] < sum[j]) {
                answer = sum[j];
            } else
                answer = sum[y];
        }

        return answer;
}
    public static int maxColumnAbsSumValue (int[][] array) {
        /*
         * Compute sum of the absolute values in per column, then display the answer from the highest row (answer)
         *
         * @Vars:
         *  sum[]   :   This stores the array of the summed up values
         *  answer  :   This stores the value attaining to the highest value in sum[]
         *
         */
        int sum[] = new int[array.length];
        int answer = 0;

        for (int x = 0; x<array.length;x++) {
            sum[x] = 0;
            for (int i = 0; i<array[x].length;i++) {
                sum[x] += Math.abs(array[i][x]);
            }
        }
        for (int y = 0, j = 1; y < sum.length && j < sum.length;y++,j++) {
            if (sum[y] < sum[j]) {
                answer = sum[j];
            } else
                answer = sum[y];
        }

        return answer;
    }

    public static void main(String[] args) {
        int array[][] = {{3,-1,4,0},{5,9,-2,6},{5,3,7,-8},{2,1,6,-2}};
        System.out.println(maxColumnAbsSumValue(array));
    }
}